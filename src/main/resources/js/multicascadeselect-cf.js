AJS.$(function () {
    var onchange = function (e) {
    	AJS.$(".child-multi-cascade-select-vis").hide();
    	AJS.$(".child-multi-cascade-select").attr('disabled', 'disabled');
        var t = AJS.$(e.target);
        var parent = t.val();
        AJS.$(".child-multi-cascade-select-" + parent).removeAttr('disabled');
        AJS.$(".child-multi-cascade-select-vis-" + parent).show();
    };
    AJS.$(".parent-multi-cascade-select").live("change keypress keydown", onchange);
});

JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context) {
	AJS.$('form[name=jiraform]').bind("before-submit", function (e) {
    	
		AJS.$('.aui-field-multicascadeselect > label').find('.icon-required').each(function() {
			
			AJS.$(this).parent().parent().find('select').each( function() {
				console.log(AJS.$(this).prev().text() + " " + AJS.$(this).data('required'));
				if (AJS.$(this).data('required')==true) {
					AJS.$(this).attr('required', 'required');
				} else {
					AJS.$(this).removeAttr('required');
				}
				
			});
			
		});
		
		AJS.$('.child-multi-cascade-select input[value=""]').attr('name', '');
		
		AJS.$('form[name=jiraform]').validate({
			errorElement: "div"
	    });
		
		return AJS.$('form[name=jiraform]').valid();
	});
  });