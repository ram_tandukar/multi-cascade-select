package com.dionetechnology.jira.plugins.multicascadeselect.ao;
 
import com.atlassian.activeobjects.tx.Transactional;
 
import java.util.List;
 
@Transactional
public interface MultiCascadeSelectService
{

	void remove(Long fieldconfig, Long customfield,int id);
	void removeUser(Long fieldconfig, Long customfield,int id);
	void removeMapping(Long fieldconfig, Long customfield,int id);
	void enable(int id);
	void disable(int id);
	void require(int id);
	void optional(int id);
	void update(int id, String value);
	void updateMapping(int id, String value);
	MultiCSCFMapping getMapping(Long option);
	void sort(Long fieldconfig, Long customfield);
	
	MultiCSCFEntity add(Long fieldconfig, Long customfield, String Value);
	MultiCSCFUser addUser(Long fieldconfig, Long customfield, String Value);
    List<MultiCSCFEntity> getMultiCascadeSelectOptions(Long fieldconfig, Long customfield);
    MultiCSCFMapping addMapping (Long fieldconfig, Long customfield, Long option, Long parent, String Assignee);
    List<MultiCSCFMapping> getMultiCascadeSelectMappings(Long fieldconfig, Long customfield);
    List<MultiCSCFUser> getMultiCascadeSelectUsers(Long fieldconfig, Long customfield);
    MultiCSCFEntity getOptionBySequence(Long fieldconfig, Long customfield, int sequence);
    MultiCSCFEntity getOptionByCustomFieldValue(Long customfield, String value);
    boolean existsUser(Long fieldconfig, Long customfield, String value);
    boolean existsValue(Long fieldconfig, Long customfield, String value);
	void moveToLast(Long fieldconfig, Long customfield, int id);
	void moveUp(Long fieldconfig, Long customfield, int id);
	void moveDown(Long fieldconfig, Long customfield, int id);
	void moveToFirst(Long fieldconfig, Long customfield, int id);
	void moveToLastMapping(Long fieldconfig, Long customfield, int id);
	void moveUpMapping(Long fieldconfig, Long customfield, int id);
	void moveDownMapping(Long fieldconfig, Long customfield, int id);
	void moveToFirstMapping(Long fieldconfig, Long customfield, int id);
    
}