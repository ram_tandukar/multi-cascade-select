package com.dionetechnology.jira.plugins.multicascadeselect.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import webwork.action.Action;
import webwork.action.ActionContext;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.MultipleSettableCustomFieldType;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.OptionUtils;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.admin.customfields.AbstractEditConfigurationItemAction;
import com.atlassian.jira.web.action.admin.customfields.EditCustomFieldDefaults;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.dionetechnology.jira.plugins.multicascadeselect.MutableOption;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCSCFEntity;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCSCFMapping;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCascadeSelectService;
import com.opensymphony.util.TextUtils;

/**
 * Forked from https://github.com/sourcesense/jira-multi-level-cascade-select
 */
@WebSudoRequired
public class EditMultiCascadeSelectOptions extends EditCustomFieldMultiCascadeSelectOptions {
  // ------------------------------------------------------------------------------------------------------
  // Properties
  
  private String value;
  private String assignee;
  private String selectedValue;
  
  private Map customFieldValuesHolder = new HashMap();

  private Options options;


  // ----------------------------------------------------------------------------------------------------
  // Dependencies

  private final IssueManager issueManager;
  private final MultiCascadeSelectService multiCascadeSelectService;
  private final OptionsManager optionsManager;
  private final UserUtil userUtil;
  

  // ----------------------------------------------------------------------------------------------------
  // Constructors
  public EditMultiCascadeSelectOptions(UserUtil userUtil, IssueManager issueManager, ManagedConfigurationItemService managedConfigurationItemService, MultiCascadeSelectService multiCascadeSelectService, OptionsManager optionsManager) {
	super(userUtil, issueManager, managedConfigurationItemService, multiCascadeSelectService, optionsManager);
	this.issueManager = issueManager;
	this.userUtil = userUtil;
    this.optionsManager = optionsManager;
    this.multiCascadeSelectService = multiCascadeSelectService;
  }

  // --------------------------------------------------------------------------------------------------
  // Action Methods
  @Override
  public String doDefault() throws Exception {
    setReturnUrl(null);
    if (!(getCustomField().getCustomFieldType() instanceof MultipleSettableCustomFieldType))
      addErrorMessage(getText("admin.errors.customfields.cannot.set.options", "'" + getCustomField().getCustomFieldType().getName() + "'"));

    EditCustomFieldDefaults.populateDefaults(getFieldConfig(), customFieldValuesHolder);

    return super.doDefault();
  }

  
  
    
    
  
  public String doEdit() throws Exception
  {
      setValue(getValue());
      return "edit";
  }


  @RequiresXsrfCheck
  public String doUpdate() throws Exception
  {
      if (!TextUtils.stringSet(value))
      {
          addError("value", getText("admin.errors.customfields.invalid.select.list.value"));
          return ERROR;
      }

      Options options = getOptions();
      Option duplicateOption = options.getOptionForValue(value, getSelectedParentOptionId());
      if (duplicateOption != null && !getSelectedOption().getOptionId().equals(duplicateOption.getOptionId()))
      {
          addError("value", getText("admin.errors.customfields.value.already.exists"));
          return ERROR;
      }
      
      // add error handling - check if valid user
      
      if (TextUtils.stringSet(assignee)&&!userUtil.userExists(assignee))
      {
          addError("assignee", getText("User Does not Exist!"));
          return ERROR;
      }
      
      getOptions().setValue(getSelectedOption(), value);
      
      MultiCSCFMapping mapping = multiCascadeSelectService.getMapping(getSelectedOption().getOptionId());
      
      if (TextUtils.stringSet(assignee)&&userUtil.userExists(assignee))
      {
    	  
          if (mapping != null) {
        	  multiCascadeSelectService.updateMapping(mapping.getID(), assignee);
          } else {
        	  
        	  multiCascadeSelectService.addMapping(getFieldConfig().getId(), getCustomField().getIdAsLong(), getSelectedOption().getOptionId(), getSelectedParentOptionId(), assignee);
          }
      } else {
    	  if(assignee==null||assignee.equals("")){
    		  if (mapping != null) {
    			  multiCascadeSelectService.removeMapping(getFieldConfig().getId(), getCustomField().getIdAsLong(),mapping.getID());
              }
    		  
    	  }
    		 
      }
    	  
      return redirectToView();
  }
  
  
  public MultiCSCFMapping getAssigneeMapping(String option) {
	  
	  return multiCascadeSelectService.getMapping(Long.valueOf(option));
	    
	  }

	  
	  public String doControllerEdit() throws Exception
	  {
	      setValue(getControllerValue());
	      return "controlleredit";
	  }


	  @RequiresXsrfCheck
	  public String doControllerUpdate() throws Exception
	  {
	      if (!TextUtils.stringSet(value))
	      {
	          addError("value", getText("admin.errors.customfields.invalid.select.list.value"));
	          return ERROR;
	      }
	      
	      Option selectedoption;
		  for (Option option : getOptions().getRootOptions()) 
		    {	
			  selectedoption = getOptions().getOptionForValue(selectedValue, option.getOptionId());
			  log.debug("value"+value);
			  Option duplicateOption = getOptions().getOptionForValue(value, option.getOptionId());
			  if (duplicateOption != null)
		      {
		          addError("value", getText("admin.errors.customfields.value.already.exists"));
		          return ERROR;
		      }
			  	
			  getOptions().setValue(selectedoption, value);
		    }
	      

		  multiCascadeSelectService.update(getControllerId(), value);

	      return redirectToView();
	  }
	  
  
	  public String getValue() {
		    return value;
		  }

		  public void setValue(String value) {
		    this.value = value;
		  }
	  public String getAssignee() {
			    return assignee;
			  }

			  public void setAssignee(String assignee) {
			    this.assignee = assignee;
			  }

		  public String getSelectedValue() {
		    return selectedValue;
		  }

		  public void setSelectedValue(String selectedValue) {
		    this.selectedValue = selectedValue;
		  }
  
}