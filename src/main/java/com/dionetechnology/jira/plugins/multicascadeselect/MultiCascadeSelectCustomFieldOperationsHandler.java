package com.dionetechnology.jira.plugins.multicascadeselect;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.customfields.impl.rest.AbstractCustomFieldOperationsHandler;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.rest.StandardOperation;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.beans.CustomFieldOptionJsonBean;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCSCFEntity;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCascadeSelectService;
import com.dionetechnology.jira.plugins.multicascadeselect.type.MultiCascadeSelectCFType;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * This class is used for the REST Operations
 * Forked from https://github.com/sourcesense/jira-multi-level-cascade-select
 */
public class MultiCascadeSelectCustomFieldOperationsHandler extends AbstractCustomFieldOperationsHandler<Map<String, Option>>
{
    private final OptionsManager optionsManager;
    private final MultiCascadeSelectService multiCascadeSelectService;

    public MultiCascadeSelectCustomFieldOperationsHandler(OptionsManager optionsManager, CustomField field, I18nHelper i18nHelper, MultiCascadeSelectService multiCascadeSelectService)
    {
        super(field, i18nHelper);
        this.optionsManager = optionsManager;
       this.multiCascadeSelectService = multiCascadeSelectService;

    }

    @Override
    public Set<String> getSupportedOperations()
    {
        return ImmutableSet.of(StandardOperation.SET.getName());
    }

    
    /**
     * Updated to 5.04 TO CHECK
     * */
    @Override
    protected Map<String, Option> handleSetOperation(IssueContext issueCtx, Issue issue, Map<String, Option> currentFieldValue, JsonData operationValue, ErrorCollection errors)
    {
        if (operationValue.isNull())
        {
            return null;
        }
        
        // The set should be an object containing a parent and optionally a child OptionBean
        // Options can be specified by Id or Value.  Id has priority as always.
        Option parent = null;
        Option child = null;
        CustomFieldOptionJsonBean bean = operationValue.convertValue(field.getId(), CustomFieldOptionJsonBean.class, errors);
        if (bean == null)
        {
            return null;
        }
        
        FieldConfig config = field.getRelevantConfig(issueCtx);
        Collection<Option> allowedOptions = optionsManager.getOptions(config);
        
        if (bean.getId() != null)
        {
            parent = findOptionById(bean.getId(), field, errors);
            if (parent == null || !allowedOptions.contains(parent))
            {
                errors.addError(field.getId(), i18nHelper.getText("rest.custom.field.option.id.invalid", bean.getId()), ErrorCollection.Reason.VALIDATION_FAILED);
                parent = null;
            }
        }
        else
        {
            String value = bean.getValue();
            if (value != null)
            {
                for (Option option : allowedOptions)
                {
                    if (option.getValue().equals(value))
                    {
                        parent = option;
                        break;
                    }
                }
                if (parent == null)
                {
                    errors.addError(field.getId(), i18nHelper.getText("rest.custom.field.option.value.invalid", value), ErrorCollection.Reason.VALIDATION_FAILED);
                }
            }
            else
            {
                errors.addError(field.getId(), i18nHelper.getText("rest.custom.field.option.parent.no.name.or.id"), ErrorCollection.Reason.VALIDATION_FAILED);
            }
        }
        Map<String, Option> options = new HashMap<String, Option>();
        if (parent != null) {
        	log.debug("parent: " + parent.getValue());
            options.put("0", parent);
            List<Option> children;
            
            int i=1;
            log.debug("config: " + config.getId().toString() + " field: " +  field.getIdAsLong().toString());
            List<MultiCSCFEntity> multiselectoptions = multiCascadeSelectService.getMultiCascadeSelectOptions(config.getId(), field.getIdAsLong());
            int index = 0;
            if (bean.getChildren()!=null) {
	            for ( CustomFieldOptionJsonBean statebeanChild : bean.getChildren())
	            {
	            	log.debug("statebeanChild: " + statebeanChild.getValue());
	            	if (!statebeanChild.getValue().equals("null")) {
	            		for ( Option childoption : parent.getChildOptions())
		                {
		            		log.debug("childoption: " + childoption.getValue());
		            		log.debug("multiselectoptions: " + multiselectoptions.get(index).getCustomValue());
		            		if(childoption.getValue().equals(multiselectoptions.get(index).getCustomValue()))	{
		            			for (Option option : childoption.getChildOptions())
		                        {
		            				
		                            if (option.getValue().equals(statebeanChild.getValue()))
		                            {
		                                child = option;
		                                break;
		                            }
		                        }
		            		
		            			if (child == null)
		    	                	{
		    	                    errors.addError(field.getId(), i18nHelper.getText("rest.custom.field.child.option.value.invalid", statebeanChild.getValue()), ErrorCollection.Reason.VALIDATION_FAILED);
		    	                	}
		            			if (child != null)
		            			{
		            				log.debug("i: " + String.valueOf(i) + " "+ child.getValue());
		            				options.put(""+i, child);
		                       
		            			}
		            			child = null;
		            		}
		            		
		                }
	            	}
	            	
	            	i++;
	            	index++;
	            }
            } else {
            	log.debug("no children");
            }
        }
        
        log.debug("size " + String.valueOf(options.size()));
        
        return options;
    }

    /**
     * compute the "currentValue" to be passed to applyOperation()
     */
    protected Map<String, Option> getInitialValue(Issue issue, ErrorCollection errors)
    {
        return (Map<String, Option>) field.getValue(issue);
    }

    /**
     * compute the "currentValue" to be passed to applyOperation()
     * @param issueCtx
     */
    protected Map<String, Option> getInitialCreateValue(IssueContext issueCtx)
    {
        FieldConfig config = field.getRelevantConfig(issueCtx);
        log.debug("initialising");
        return (Map<String, Option>) field.getCustomFieldType().getDefaultValue(config);
    }

    /**
     * Updated to 5.0.4 TO CHECK
     */
    @Override
    protected void finaliseOperation(Map<String, Option> finalValue, IssueInputParameters parameters, ErrorCollection errors)
    {
    	String parentId;
    	Option currentOpt;
        if (finalValue != null)
        {
          for (Map.Entry<String, Option> optionmap : finalValue.entrySet()) {	
        	  		
        	  parentId = optionmap.getKey();
              currentOpt = optionmap.getValue();
        		  parameters.addCustomFieldValue(field.getId(), currentOpt.getOptionId().toString());
                  parameters.addCustomFieldValue(field.getId()+ ":"+parentId, currentOpt.getOptionId().toString());
                  
          }
        }
        else
        {
            parameters.addCustomFieldValue(field.getId(), null);
        }
    }

    /**
     * Returns the Option that has the given <code>optionId</code>, or null if there is no Option with the given id or
     * if the given id is not valid. When returning null, the
     *
     * @param optionId a String containing an option id
     * @param field the Field
     * @param errors an ErrorCollection where errors will be added
     * @return an Option or null
     */
    private Option findOptionById(String optionId, CustomField field, ErrorCollection errors)
    {
        try
        {
            return optionsManager.findByOptionId(Long.valueOf(optionId));
        }
        catch (NumberFormatException e)
        {
            errors.addError(field.getId(),  i18nHelper.getText("rest.custom.field.option.id.invalid", optionId), ErrorCollection.Reason.VALIDATION_FAILED);
            return null;
        }
    }
    
    private final Logger log = Logger.getLogger(MultiCascadeSelectCustomFieldOperationsHandler.class);
}

