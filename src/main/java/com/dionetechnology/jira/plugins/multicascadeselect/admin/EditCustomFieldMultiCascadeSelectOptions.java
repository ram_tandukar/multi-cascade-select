package com.dionetechnology.jira.plugins.multicascadeselect.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import webwork.action.Action;
import webwork.action.ActionContext;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.MultipleSettableCustomFieldType;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.OptionUtils;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.admin.customfields.AbstractEditConfigurationItemAction;
import com.atlassian.jira.web.action.admin.customfields.EditCustomFieldDefaults;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.dionetechnology.jira.plugins.multicascadeselect.MutableOption;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCSCFEntity;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCSCFMapping;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCSCFUser;
import com.dionetechnology.jira.plugins.multicascadeselect.ao.MultiCascadeSelectService;
import com.opensymphony.util.TextUtils;

/**
 * Forked from https://github.com/sourcesense/jira-multi-level-cascade-select
 */
@WebSudoRequired
public class EditCustomFieldMultiCascadeSelectOptions extends AbstractEditConfigurationItemAction {
  // ------------------------------------------------------------------------------------------------------
  // Properties
  
  private String addValue;
  private String uservalue;
  private String assignee;
  private String value;
  private int userid;
  private String selectedValue;
  private String childview;
  private String grandchildview;
  private boolean confirm;
  private int controllerid;
  private String controllerValue;
  private String templateview;
  private Long templateparent;
  private String controlValue;

  private Collection<String> hlFields;
  
  private String focus;
  
  private Long selectedParentOptionId;

  private Map customFieldValuesHolder = new HashMap();

  private Options options;
  private Options parentoptions;

  private Object defaultValues;

  private static final String NEW_OPTION_POSITION_PREFIX = "newOptionPosition_";

  private static final String NEW_LABEL_PREFIX = "newLabel_";

  private static final String ACTION_NAME = "EditMultiCascadeSelect";
  private static final String ACTION_NAME_CHILD = "EditMultiCascadeSelectChild";

  // ----------------------------------------------------------------------------------------------------
  // Dependencies

  private final IssueManager issueManager;
  private final MultiCascadeSelectService multiCascadeSelectService;
  private final OptionsManager optionsManager;
  private final UserUtil userUtil;
  

  // ----------------------------------------------------------------------------------------------------
  // Constructors
  public EditCustomFieldMultiCascadeSelectOptions(UserUtil userUtil, IssueManager issueManager, ManagedConfigurationItemService managedConfigurationItemService, MultiCascadeSelectService multiCascadeSelectService, OptionsManager optionsManager) {
	super(managedConfigurationItemService);
	this.issueManager = issueManager;
    this.optionsManager = optionsManager;
	this.userUtil = userUtil;
    this.hlFields = new LinkedList<String>();
    this.multiCascadeSelectService = multiCascadeSelectService;
  }

  // --------------------------------------------------------------------------------------------------
  // Action Methods
  @Override
  public String doDefault() throws Exception {
    setReturnUrl(null);
    if (!(getCustomField().getCustomFieldType() instanceof MultipleSettableCustomFieldType))
      addErrorMessage(getText("admin.errors.customfields.cannot.set.options", "'" + getCustomField().getCustomFieldType().getName() + "'"));

    EditCustomFieldDefaults.populateDefaults(getFieldConfig(), customFieldValuesHolder);

    return super.doDefault();
  }

  @Override
  protected void doValidation() {
    if (getCustomField() == null) {
      addErrorMessage(getText("admin.errors.customfields.no.field.selected.for.edit"));
    }
  }

  public String doConfigureOption() {
    Map parameters = ActionContext.getParameters();
    if (parameters.containsKey("moveOptionsToPosition")) {
      // Move the options to a different position
    
      return changeOptionPositions(parameters);
    }

    if (parameters.containsKey("saveLabel")) {
      // Move the options to a different position
      return changeOptionsLabel(parameters);
    }

    throw new IllegalStateException("Unknown operation.");
  }

  /**
   * operates the change of Options Position (Up and Down in the edit config scenario) If the change
   * of position is not possible,an error is returned.
   * 
   * @param parameters
   * @return
   */
  private String changeOptionPositions(Map<String, Object> parameters) {
    Map<Integer, Option> optionPositions = new TreeMap<Integer, Option>();
    // Loop through the submitted parameters and find out which options to move
    for (String paramName : parameters.keySet()) {
      if (paramName.startsWith(NEW_OPTION_POSITION_PREFIX) && TextUtils.stringSet(getTextValueFromParams(paramName))) {
        String fieldId = paramName.substring(NEW_OPTION_POSITION_PREFIX.length());
        Integer newOptionPosition = null;
        try {
          newOptionPosition = Integer.valueOf(getTextValueFromParams(paramName));
          Integer newIndex = new Integer(newOptionPosition.intValue() - 1);
          if (newOptionPosition.intValue() <= 0 || newOptionPosition.intValue() > getDisplayOptions().size()) {
            addError(paramName, getText("admin.errors.invalid.position"));
          } else if (optionPositions.containsKey(newIndex)) {
            addError(paramName, getText("admin.errors.invalid.position"));
          } else {
            optionPositions.put(newIndex, getOptions().getOptionById(Long.decode(fieldId)));
          }
        } catch (NumberFormatException e) {
          addError(paramName, getText("admin.errors.invalid.position"));
        }
      }
    }

    if (!invalidInput()) {
      getOptions().moveOptionToPosition(optionPositions);
      // Mark fields for highlighting
      for (Iterator<Option> iterator = optionPositions.values().iterator(); iterator.hasNext();) {
        populateHlField(iterator.next());
      }

      return redirectToView();
    }

    return getResult();
  }

  /**
   * operates the function of renaming the label of an option
   * 
   * @param parameters
   * @return
   */
  private String changeOptionsLabel(Map<String, Object> parameters) {
    List<Option> options = new ArrayList<Option>();
    for (String paramName : parameters.keySet()) {
      if (paramName.startsWith(NEW_LABEL_PREFIX) && TextUtils.stringSet(getTextValueFromParams(paramName))) {
        String fieldId = paramName.substring(NEW_LABEL_PREFIX.length());
        String value = getTextValueFromParams(paramName);
        MutableOption option = new MutableOption(optionsManager.findByOptionId(OptionUtils.safeParseLong(fieldId)));
        if (!option.getValue().equals(value)) {
          option.setValue(value);
          options.add(option);
        }
      }
    }
    if (!options.isEmpty()) {
      optionsManager.updateOptions(options);
      populateHlField(options);
    }
    return redirectToView();
  }

  /**
   * operates a redirection from a view of the Custom Field to another.
   * 
   * @return
   */ 
  protected String redirectToView() {
	  log.debug("redirecting to view");
    StringBuffer redirectUrl = new StringBuffer(ACTION_NAME).append("!default.jspa?fieldConfigId=").append(getFieldConfigId());
    
    if(getTemplateView()!=null) {
			redirectUrl = new StringBuffer(ACTION_NAME).append("!default.jspa?fieldConfigId=").append(getFieldConfigId());
			redirectUrl.append("&selectedParentOptionId=");
    }else if(getGrandChildView()!=null) {
    	
		redirectUrl = new StringBuffer(ACTION_NAME_CHILD).append("!default.jspa?fieldConfigId=").append(getFieldConfigId());
        redirectUrl.append("&selectedParentOptionId=" + getSelectedParentOption().getParentOption().getOptionId());
        log.debug("redirectUrl " + redirectUrl);
    }
    else if(getChildView()!=null) {
		redirectUrl = new StringBuffer(ACTION_NAME_CHILD).append("!default.jspa?fieldConfigId=").append(getFieldConfigId());
    	redirectUrl.append("&selectedParentOptionId=" + getSelectedParentOptionId());
    }
    else {
       redirectUrl.append("&selectedParentOptionId=" + getSelectedParentOptionId());
    }
     
      for (Iterator<String> iterator = hlFields.iterator(); iterator.hasNext();) {
        redirectUrl.append("&currentOptions=").append(iterator.next());
      }
    if (focus!=null) {
    	redirectUrl.append("#" + focus);
    }
    return forceRedirect(redirectUrl.toString());
  }

  /**
   * Extracts the text value from an input param
   * 
   * @param newPositionFieldName
   * @return
   */
  private String getTextValueFromParams(String newPositionFieldName) {
    String[] newFieldPositionArray = (String[]) ActionContext.getParameters().get(newPositionFieldName);

    if (newFieldPositionArray != null && newFieldPositionArray.length > 0)
      return newFieldPositionArray[0];
    else
      return "";
  }

  public String doAdd() throws Exception {
    doValidation();
    if (!TextUtils.stringSet(addValue)) {
      addError("addValue", getText("admin.errors.customfields.invalid.select.list.value"));
    }

    if (invalidInput())
      return getResult();

    Options options = getOptions();
    
    if (options.getOptionForValue(getAddValue(), getSelectedParentOptionId()) != null) {
      
      
      if(getSelectedParentOptionId()==null) {
    	  focus = "parent";
    	  addError("addValue", getText("admin.errors.customfields.value.already.exists"));
      } else {
      	focus = "child_" + getSelectedParentOptionId();
      	addError("childaddValue_" + String.valueOf(getSelectedParentOptionId()), getText("admin.errors.customfields.value.already.exists"));
      }
      log.debug("redirecting to view");
      return ERROR;
      
    }
    
    // set the options
    Option newoption = options.addOption(options.getOptionById(getSelectedParentOptionId()), getAddValue());

    if (!getDisplayOptions().isEmpty())
      hlFields.add(getAddValue());
    
    if(getSelectedParentOptionId()==null) {
    	
    	for (MultiCSCFEntity childoption : getControllerOptions()) 
	    {
    		
    		Option nextoption = options.addOption(newoption, childoption.getCustomValue());
    		options.moveToLastSequence(nextoption);
	    }
    	focus = "parent";
    	
    } else {
    	focus = "child_" + getSelectedParentOptionId();
    }

    return redirectToView();

  }

  public String doSort() throws Exception {
    getOptions().sortOptionsByValue(getSelectedParentOption());

    return redirectToView();
  }

  public String doMoveToFirst() throws Exception {
    populateHlField(getSelectedOption());
    getOptions().moveToStartSequence(getSelectedOption());

    return redirectToView();
  }

  public String doMoveUp() throws Exception {
    populateHlField(getSelectedOption());
    getOptions().decrementSequence(getSelectedOption());

    return redirectToView();
  }

  public String doMoveDown() throws Exception {
    populateHlField(getSelectedOption());
    getOptions().incrementSequence(getSelectedOption());

    return redirectToView();
  }

  public String doMoveToLast() throws Exception {
    populateHlField(getSelectedOption());
    getOptions().moveToLastSequence(getSelectedOption());

    return redirectToView();
  }

  private void populateHlField(Option option) {
    hlFields.add(option.getValue());
  }

  private void populateHlField(List<Option> options) {
    for (Option opt : options) {
      populateHlField(opt);
    }
  }

  public String getNewPositionTextBoxName(int optionId) {
    return NEW_OPTION_POSITION_PREFIX + optionId;
  }

  public String getNewLabelTextBoxName(int optionId) {
    return NEW_LABEL_PREFIX + optionId;
  }

  public String getNewPositionValue(int optionId) {
    return getTextValueFromParams(getNewPositionTextBoxName(optionId));
  }

  public String doRemove() throws Exception {
	  setValue(getSelectedOption().getValue());
    if (!confirm)
      return "confirmdelete";

    removeValuesFromIssues();
    getOptions().removeOption(getSelectedOption());
    return redirectToView();
  }
  
  @RequiresXsrfCheck
  public String doDisable() throws Exception
  {
      getOptions().disableOption(getSelectedOption());

      return redirectToView();
  }

  @RequiresXsrfCheck
  public String doEnable() throws Exception
  {
      getOptions().enableOption(getSelectedOption());

      return redirectToView();
  }

  
  public String doEdit() throws Exception
  {
      setValue(getSelectedOption().getValue());
      return "edit";
  }


  @RequiresXsrfCheck
  public String doUpdate() throws Exception
  {
      if (!TextUtils.stringSet(value))
      {
          addError("value", getText("admin.errors.customfields.invalid.select.list.value"));
          return "error";
      }

      Options options = getOptions();
      Option duplicateOption = options.getOptionForValue(value, getSelectedParentOptionId());
      if (duplicateOption != null && !getSelectedOption().getOptionId().equals(duplicateOption.getOptionId()))
      {
          addError("value", getText("admin.errors.customfields.value.already.exists"));
          return ERROR;
      }
      

      
      getOptions().setValue(getSelectedOption(), value);
      
      if (TextUtils.stringSet(assignee)&&!userUtil.userExists(assignee))
      {
          addError("assignee", getText("User Does not Exist!"));
          return ERROR;
      }
      
      MultiCSCFMapping mapping = multiCascadeSelectService.getMapping(getSelectedOption().getOptionId());
      
      if (TextUtils.stringSet(assignee)&&userUtil.userExists(assignee))
      {
    	  
          if (mapping != null) {
        	  multiCascadeSelectService.updateMapping(mapping.getID(), assignee);
          } else {
        	  
        	  multiCascadeSelectService.addMapping(getFieldConfig().getId(), getCustomField().getIdAsLong(), getSelectedOption().getOptionId(), getSelectedParentOptionId(), assignee);
          }
      } else {
    	  if(assignee==null||assignee.equals("")){
    		  if (mapping != null) {
    			  multiCascadeSelectService.removeMapping(getFieldConfig().getId(), getCustomField().getIdAsLong(),mapping.getID());
              }
    		  
    	  }
    		 
      }


      return redirectToView();
  }

  @Override
  protected String doExecute() throws Exception {
    return INPUT;
  }
  
//---------------------------------------------------------------------- Child Controller Commands
  
  public String doControllerAdd() throws Exception {

	    if (!TextUtils.stringSet(controlValue)) {
	      addError("templateAddValue", getText("admin.errors.customfields.invalid.select.list.value"));
	    }

	    if (invalidInput())
	      return getResult();

	    // getControllerOptions()
	    Options options = getOptions();
	    
	    
	    //  Checkoptionvalue(fieldconfig, customfield, value)
	    if (multiCascadeSelectService.existsValue(getFieldConfigId(), getCustomField().getIdAsLong(), getControlValue())) {
	      addError("templateAddValue", getText("admin.errors.customfields.value.already.exists"));
	      return Action.ERROR;
	    }
	    
	    // set the options
	    // 
	    multiCascadeSelectService.add(getFieldConfigId(), getCustomField().getIdAsLong(), getControlValue());
	    // loop through parent options and add child
	    
	    for (Option option : options.getRootOptions()) 
	    {
	    	options.addOption(option,getControlValue());
	    }
	    
	    if (!getDisplayOptions().isEmpty())
	      hlFields.add(getControlValue());

	    focus = "child";
	    return redirectToView();

	  }
  
  public String doAddUser() throws Exception {

	    if (!TextUtils.stringSet(getAddValueUser())) {
	      addError("uservalue", getText("admin.errors.customfields.invalid.select.list.value"));
	      return Action.ERROR;
	    }

	    //  Checkoptionvalue(fieldconfig, customfield, value)
	    if (multiCascadeSelectService.existsUser(getFieldConfigId(), getCustomField().getIdAsLong(), getAddValueUser())) {
	      addError("uservalue", "User Already Added");
	      return Action.ERROR;
	    }
	    
	    // set the options
	    // 
	    multiCascadeSelectService.addUser(getFieldConfigId(), getCustomField().getIdAsLong(), getAddValueUser());
	    // loop through parent options and add child
	    
	    
	    focus = "user";
	    return redirectToView();

	  }
  
     public String doRemoveUser() throws Exception {
	     
    	 multiCascadeSelectService.removeUser(getFieldConfigId(), getCustomField().getIdAsLong(), getUserId());
       return redirectToView();
     }

	  public String doControllerSort() throws Exception {
		  
		  multiCascadeSelectService.sort(getFieldConfigId(), getCustomField().getIdAsLong());
		  
		  for (Option option : getOptions().getRootOptions()) 
		    {
			  getOptions().sortOptionsByValue(option);
		    }

	    return redirectToView();
	  }

	  public String doControllerMoveToFirst() throws Exception {
	    
		multiCascadeSelectService.moveToFirst(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());
	    Option selectedoption;
		for (Option option : getOptions().getRootOptions()) 
	    {	
			selectedoption = getOptions().getOptionForValue(getControllerValue(), option.getOptionId());
			getOptions().moveToStartSequence(selectedoption);
	    }
	    
	    return redirectToView();
	  }

	  public String doControllerMoveUp() throws Exception {
	    
		  multiCascadeSelectService.moveUp(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());
		  
		  Option selectedoption;
		  for (Option option : getOptions().getRootOptions()) 
		    {	 
			  selectedoption = getOptions().getOptionForValue(getControllerValue(), option.getOptionId());
			  getOptions().decrementSequence(selectedoption);
		    }
		  
	    return redirectToView();
	  }

	  public String doControllerMoveDown() throws Exception {
		  
		  multiCascadeSelectService.moveDown(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());
		  Option selectedoption;
		  for (Option option : getOptions().getRootOptions()) 
		    {	 
			  selectedoption = getOptions().getOptionForValue(getControllerValue(), option.getOptionId());
			  getOptions().incrementSequence(selectedoption);
		    }

		  return redirectToView();
	  }

	  public String doControllerMoveToLast() throws Exception {
	    
	    multiCascadeSelectService.moveToLast(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());
	    Option selectedoption;
	    
		for (Option option : getOptions().getRootOptions()) 
	    {	
			selectedoption = getOptions().getOptionForValue(getControllerValue(), option.getOptionId());
			getOptions().moveToLastSequence(selectedoption);
	    }
	    
	    return redirectToView();
	  }

	  public String doControllerRemove() throws Exception {
		 
		setValue(getControllerValue());
	    if (!confirm)
	      return "confirmdeletecontroller";
	    Option selectedoption;
	    
		for (Option option : getOptions().getRootOptions()) 
	    {	
			selectedoption = getOptions().getOptionForValue(getSelectedValue(), option.getOptionId());
			removeValuesFromIssues(selectedoption);
			getOptions().removeOption(selectedoption);
	    }
	    
		multiCascadeSelectService.remove(getFieldConfigId(), getCustomField().getIdAsLong(),getControllerId());
	    
	    return redirectToView();
	  }
	  
	  @RequiresXsrfCheck
	  public String doControllerDisable() throws Exception
	  {
			
		  multiCascadeSelectService.disable(getControllerId());
		  Option selectedoption;
		  for (Option option : getOptions().getRootOptions()) 
		    {	
			  	selectedoption = getOptions().getOptionForValue(getControllerValue(), option.getOptionId());
				getOptions().disableOption(selectedoption);
		    }
		
	      return redirectToView();
	  }

	  @RequiresXsrfCheck
	  public String doControllerRequired() throws Exception
	  {
		  multiCascadeSelectService.require(getControllerId());
		 
	      return redirectToView();
	  }
	  
	  @RequiresXsrfCheck
	  public String doControllerOptional() throws Exception
	  {
		  multiCascadeSelectService.optional(getControllerId());
		 
	      return redirectToView();
	  }
	  
	  @RequiresXsrfCheck
	  public String doControllerEnable() throws Exception
	  {
		  multiCascadeSelectService.enable(getControllerId());
		  Option selectedoption;
		  for (Option option : getOptions().getRootOptions()) 
		    {	
			  	selectedoption = getOptions().getOptionForValue(getControllerValue(), option.getOptionId());
				getOptions().enableOption(selectedoption);
		    }

	      return redirectToView();
	  }

	  
	  public String doControllerEdit() throws Exception
	  {
	      setValue(getControllerValue());
	      return "editcontroller";
	  }


	  @RequiresXsrfCheck
	  public String doControllerUpdate() throws Exception
	  {
	      if (!TextUtils.stringSet(value))
	      {
	          addError("value", getText("admin.errors.customfields.invalid.select.list.value"));
	          return ERROR;
	      }
	      
	      Option selectedoption;
		  for (Option option : getOptions().getRootOptions()) 
		    {	
			  selectedoption = getOptions().getOptionForValue(selectedValue, option.getOptionId());
			  Option duplicateOption = options.getOptionForValue(value, option.getOptionId());
			  if (duplicateOption != null)
		      {
		          addError("value", getText("admin.errors.customfields.value.already.exists"));
		          return ERROR;
		      }
			  	
			  getOptions().setValue(selectedoption, value);
		    }
	      

		  multiCascadeSelectService.update(getControllerId(), value);

	      return redirectToView();
	  }
	  
	  public String doMoveToFirstMapping() throws Exception {
		  
		  multiCascadeSelectService.moveToFirstMapping(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());

		    return redirectToView();
      }

	  public String doMoveUpMapping() throws Exception {
		    
		  multiCascadeSelectService.moveUpMapping(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());

		  
		    return redirectToView();
	  }

	  public String doMoveDownMapping() throws Exception {
		  multiCascadeSelectService.moveDownMapping(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());


		    return redirectToView();
		  }

	  public String doMoveToLastMapping() throws Exception {

		  multiCascadeSelectService.moveToLastMapping(getFieldConfigId(), getCustomField().getIdAsLong(), getControllerId());

		  
		    return redirectToView();
		  }
  
  
  // --------------------------------------------------------------------------------------------------
  // Helper Methods

  private void removeValuesFromIssues() {
    Collection<Issue> issues = getAffectedIssues();
    for (Issue issue : issues) {
      MultipleSettableCustomFieldType customFieldType = (MultipleSettableCustomFieldType) getCustomField().getCustomFieldType();
      customFieldType.removeValue(getCustomField(), issue, getSelectedOption());
    }
  }
  
  private void removeValuesFromIssues(Option option) {
	    Collection<Issue> issues = getAffectedIssues(option);
	    for (Issue issue : issues) {
	      MultipleSettableCustomFieldType customFieldType = (MultipleSettableCustomFieldType) getCustomField().getCustomFieldType();
	      customFieldType.removeValue(getCustomField(), issue, option);
	    }
	  }

  public Collection<Issue> getAffectedIssues() {
    final MultipleSettableCustomFieldType customFieldType = (MultipleSettableCustomFieldType) getCustomField().getCustomFieldType();
    Set<Long> ids = customFieldType.getIssueIdsWithValue(getCustomField(), getSelectedOption());
    Collection<Issue> issues = new ArrayList<Issue>(ids.size());
    for (Long id : ids) {
      final Issue issue = IssueImpl.getIssueObject(issueManager.getIssue(id));
      final FieldConfig relevantConfigFromGv = getCustomField().getRelevantConfig(issue);
      if (getFieldConfig().equals(relevantConfigFromGv)) {
        issues.add(issue);
      }
    }
    return issues;
  }
  
  public Collection<Issue> getAffectedIssues(Option option) {
	    final MultipleSettableCustomFieldType customFieldType = (MultipleSettableCustomFieldType) getCustomField().getCustomFieldType();
	    Set<Long> ids = customFieldType.getIssueIdsWithValue(getCustomField(), option);
	    Collection<Issue> issues = new ArrayList<Issue>(ids.size());
	    for (Long id : ids) {
	      final Issue issue = IssueImpl.getIssueObject(issueManager.getIssue(id));
	      final FieldConfig relevantConfigFromGv = getCustomField().getRelevantConfig(issue);
	      if (getFieldConfig().equals(relevantConfigFromGv)) {
	        issues.add(issue);
	      }
	    }
	    return issues;
	  }

  // -------------------------------------------------------------------------------------------
  // Non-trivial accessors

  // ------------------------------------------------------------------------------------------
  // Private Helper Methods

  public Options getOptions() {
    if (options == null) {
      Long selectedParentOptionId = getSelectedParentOptionId();
      options = getCustomField().getOptions(selectedParentOptionId != null ? selectedParentOptionId.toString() : null, getFieldConfig(), null);
    }
    return options;
  }
  
  public Collection<Option> getDisplayOptions() {
    final Options options = getOptions();
    Collection<Option> result = new ArrayList<Option>();
    final Option parentOption = options.getOptionById(getSelectedParentOptionId());
    if (parentOption != null) {
      return parentOption.getChildOptions();
    } else {
      return options;
    }
  }
  
  public MultiCSCFMapping getAssignee(Long option) {
	  
	  return multiCascadeSelectService.getMapping(option);
	    
	  }
  
  public Collection<MultiCSCFMapping> getMultiCascadeSelectMappings() {
	  
	  return multiCascadeSelectService.getMultiCascadeSelectMappings(getFieldConfigId(), getCustomField().getIdAsLong());
	    
	  }
  
  public Collection<MultiCSCFUser> getMultiCascadeSelectUsers() {
	  
	  return multiCascadeSelectService.getMultiCascadeSelectUsers(getFieldConfigId(), getCustomField().getIdAsLong());
	    
	  }
  
  public Collection<Option> getRootOptions() {
	    final Options options = getOptions();
	    
	    return options.getRootOptions();
	  }
  
  public Collection<MultiCSCFEntity> getControllerOptions() {

	    return multiCascadeSelectService.getMultiCascadeSelectOptions(getFieldConfigId(), getCustomField().getIdAsLong());
	    
	  }
  
  public Collection<Option> getDisplayChildOptions(Long parent) {
	    final Options options = getOptions();
	    Collection<Option> result = new ArrayList<Option>();
	    final Option parentOption = options.getOptionById(parent);
	    if (parentOption != null) {
	      return parentOption.getChildOptions();
	    } else {
	      return options;
	    }
	  }

  public Option getSelectedOption() {
    return getOptions().getOptionById(OptionUtils.safeParseLong(getSelectedValue()));
  }
  
  public Option getSelectedOption(Long optionid) {
	    return getOptions().getOptionById(optionid);
	  }

  public Option getSelectedParentOption() {
    return getOptions().getOptionById((getSelectedParentOptionId()));
  }
  

  public Object getDefaultValues() {
	  
    if (defaultValues == null) {
      Object dbDefaultValues = getCustomField().getCustomFieldType().getDefaultValue(getFieldConfig());
      if (dbDefaultValues instanceof String) {
        final Collection<Object> tempCollection = new ArrayList<Object>(1);
        tempCollection.add(dbDefaultValues);
        defaultValues = tempCollection;
      } else {
        defaultValues = dbDefaultValues;
      }
    }

    return defaultValues;
  }

  public boolean isDefaultValue(String value) {
    Object defaults = getDefaultValues();

    if (defaults instanceof Collection) {
      Collection defCollection = (Collection) defaults;

      Option option = options.getOptionById(OptionUtils.safeParseLong(value));

      if (option != null) {
        return defCollection.contains(option.getValue());
      } else {
        return false;
      }
    } else if (defaults instanceof CustomFieldParams) {
      CustomFieldParams fieldParams = (CustomFieldParams) defaults;
      Collection allFieldValues = fieldParams.getAllValues();
      for (Object defaultOptionId : allFieldValues) {
        if (value != null && value.equals(defaultOptionId)) {
          return true;
        } else if (defaultOptionId instanceof Option) {
          Option option = (Option) defaultOptionId;
          if (option.getOptionId().toString().equals(value))
            return true;
        }

      }
    }

    return false;
  }

  public int getButtonRowSize() {
    int rowSize = 2;
    if (getDisplayOptions().size() > 1)
      rowSize++;

    return rowSize;
  }

  public boolean isCascadingSelect() {
    return getCustomField().getCustomFieldType() instanceof CascadingSelectCFType;
  }

  private String getRedirectUrl() {
    if (getSelectedParentOptionId() == null) {
      return getBaseUrl();
    } else {
      return getUrlWithParent("default");
    }
  }

  private String getBaseUrl() {
    return getBaseUrl("default");
  }

  private String getBaseUrl(String action) {
    return ACTION_NAME + "!" + action + ".jspa?fieldConfigId=" + getFieldConfig().getId();
  }

  public String getSelectedParentOptionUrlPreifx() {
    return getBaseUrl() + "&selectedParentOptionId=";
  }

  public String getSelectedParentOptionUrlPrefix(String action) {
    return getBaseUrl(action) + "&selectedParentOptionId=";
  }

  public String getDoActionUrl(Option option, String action) {

    return getUrlWithParent(action) + "&selectedValue=" + (option != null ? option.getOptionId().toString() : "");
  }

  public String getUrlWithParent(String action) {
    if (getSelectedParentOptionId() == null) {
      return getBaseUrl(action);
    } else {
      return getBaseUrl(action) + "&selectedParentOptionId=" + getSelectedParentOptionId();

    }
  }
  
//---------------------------------------------------------------------- Child Controller Options
  
  
  public Collection<MultiCSCFEntity> getChildControllerOptions() {
		
	  Collection<MultiCSCFEntity> options = null;

	  return options;
	  
  }

  // ---------------------------------------------------------------------- Accessors & Mutators for
  // action properties
  public String getAddValue() {
    return addValue;
  }

  public void setAddValue(String addValue) {
    this.addValue = addValue;
  }
  
  public String getAddValueUser() {
	    return addValue;
  }

  public void setAddValueUser(String addValue) {
	    this.addValue = addValue;
  }

  public String getSelectedValue() {
    return selectedValue;
  }

  public void setSelectedValue(String selectedValue) {
    this.selectedValue = selectedValue;
  }
  
  public String getControllerValue() {
	    return controllerValue;
	  }

  public void setControllerValue(String controllerValue) {
	    this.controllerValue = controllerValue;
  }
  
public String getControlValue() {
	return controlValue;
}

public void setControlValue(String controlValue) {
	this.controlValue = controlValue;
}
  
  public int getControllerId() {
	    return controllerid;
	  }

  public void setControllerId(int id) {
	    this.controllerid = id;
	  }

  public Long getSelectedParentOptionId() {
    return selectedParentOptionId;
  }

  public void setSelectedParentOptionId(Long selectedParentOptionId) {
    this.selectedParentOptionId = selectedParentOptionId;
  }

  public void setConfirm(boolean confirm) {
    this.confirm = confirm;
  }
  
  public String getValue()
  {
      return value;
  }

  public void setValue(String value)
  {
      this.value = value;
  }
  
  public int getUserId()
  {
      return userid;
  }

  public void setUserId(int id)
  {
      this.userid = id;
  }

  
  public String getFocus()
  {
      return focus;
  }

  public void setFocus(String focus)
  {
      this.focus = focus;
  }
  
  public String getAssignee() {
	    return assignee;
	  }

	  public void setAssignee(String assignee) {
	    this.assignee = assignee;
	  }
  
  public String getChildView()
  {
      return childview;
  }

  public void setChildView(String childview)
  {
      this.childview = childview;
  }
  
  public String getTemplateView()
  {
      return templateview;
  }

  public void setTemplateView(String templateview)
  {
      this.templateview = templateview;
  }
  
  public Long getTemplateParent()
  {
      return templateparent;
  }

  public void setTemplateParent(Long templateparent)
  {
      this.templateparent = templateparent;
  }
  
  public String getGrandChildView()
  {
      return grandchildview;
  }

  public void setGrandChildView(String grandchildview)
  {
      this.grandchildview = grandchildview;
  }


  public Collection<String> getHlOptions() {
    return hlFields;
  }

  public void setCurrentOptions(String[] currentFields) {
    this.hlFields = Arrays.asList(currentFields);
  }
}