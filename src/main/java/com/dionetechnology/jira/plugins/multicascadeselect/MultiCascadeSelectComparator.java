package com.dionetechnology.jira.plugins.multicascadeselect;

import java.util.Comparator;

/**
 * 
 * Forked from https://github.com/sourcesense/jira-multi-level-cascade-select
 */
public class MultiCascadeSelectComparator implements Comparator<MultiCascadeSelectValue> {

    @Override
    public int compare(MultiCascadeSelectValue o1, MultiCascadeSelectValue o2) {
      if (o1 == null && o2 == null)
      {
          return 0;
      }
      else if (o1 == null||o1.getSearchValue()==null)
      {
          return 1;
      }
      else if (o2 == null||o2.getSearchValue()==null)
      {
          return -1;
      }
      return o1.toString().compareTo(o2.toString());
    }
}
