package com.dionetechnology.jira.plugins.multicascadeselect.ao;
 
import net.java.ao.Entity;
import net.java.ao.Preload;
 
@Preload
public interface MultiCSCFEntity extends Entity
{
    Long getCustomField();
 
    void setCustomField(Long cf);
    
    Long getFieldConfig();
    
    void setFieldConfig(Long fieldconfig);
    
    Long getParentOption();
    
    void setParentOption(Long parent);
    
    Integer getSequence();
    
    void setSequence(Integer seq);
    
    String getCustomValue();
    
    void setCustomValue(String cv);
    
    String getOptionType();
    
    void setOptionType(String type);
    
    String getDisabled();
    
    void setDisabled(String enabled);
    
    String getRequired();
    
    void setRequired(String required);
    
    
}