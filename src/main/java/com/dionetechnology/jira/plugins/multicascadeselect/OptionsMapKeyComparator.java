package com.dionetechnology.jira.plugins.multicascadeselect;

import java.util.Comparator;

/**
 * Forked from https://github.com/sourcesense/jira-multi-level-cascade-select
 */

public class OptionsMapKeyComparator implements Comparator<String> {
    public int compare(String o1, String o2) {
      if (o1==null || o2==null) {
        if (o1==o2) {
            return 0;
        }
        if (o1==null) {
            return -1;
        }
        return 1;
    }

    return o1.toString().compareTo(o2.toString());
    }
}
