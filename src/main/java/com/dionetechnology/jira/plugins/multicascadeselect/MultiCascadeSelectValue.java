package com.dionetechnology.jira.plugins.multicascadeselect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;

/**
 *
 * Forked from https://github.com/sourcesense/jira-multi-level-cascade-select
 */

public class MultiCascadeSelectValue {
    private String value;
    private OptionsManager optionsManager;

    private static final Logger log = Logger.getLogger(MultiCascadeSelectValue.class);

    public MultiCascadeSelectValue(OptionsManager optionsManager, String value) {
        this.optionsManager = optionsManager;
        this.value = value;
    }

    public String getSearchValue() {
        return value;
    }



    @Override
    public String toString() {
        return value;
    }


}