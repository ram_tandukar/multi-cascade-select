package com.dionetechnology.jira.plugins.multicascadeselect.ao;
 
import com.atlassian.activeobjects.external.ActiveObjects;
import com.dionetechnology.jira.plugins.multicascadeselect.type.MultiCascadeSelectCFType;
 
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import net.java.ao.Query;
 
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
 
public class MultiCascadeSelectServiceImpl implements MultiCascadeSelectService
{
    private final ActiveObjects ao;
 
    public MultiCascadeSelectServiceImpl(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    
    @Override
    public void remove(Long fieldconfig, Long customfield, int id)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	int sequence = optionEntity.getSequence();
    	int count = ao.count(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield));
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE >= ?", fieldconfig, customfield, sequence)));
    	for (MultiCSCFEntity option : options) {
    		if(option.getID()==id) {
    			option.setSequence(count-1);
    		} else
    		{
    		 option.setSequence(option.getSequence() - 1);
    		}
    		option.save();
        }
    	ao.delete(optionEntity);
	
    }
    
    @Override
    public void removeMapping(Long fieldconfig, Long customfield, int id)
    {
    	MultiCSCFMapping optionEntity = ao.get(MultiCSCFMapping.class,id);
    	int sequence = optionEntity.getSequence();
    	int count = ao.count(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield));
    	ArrayList<MultiCSCFMapping> options = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE >= ?", fieldconfig, customfield, sequence)));
    	for (MultiCSCFMapping option : options) {
    		if(option.getID()==id) {
    			option.setSequence(count-1);
    		} else
    		{
    		 option.setSequence(option.getSequence() - 1);
    		}
    		option.save();
        }
    	ao.delete(optionEntity);
	
    }
    

	@Override
	public void removeUser(Long fieldconfig, Long customfield, int id) {
		MultiCSCFUser optionEntity = ao.get(MultiCSCFUser.class,id);
		ao.delete(optionEntity);
	}


    
    @Override
    public void enable(int id)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	optionEntity.setDisabled("false");
    	optionEntity.save();	
    }

    @Override
    public void disable(int id)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	optionEntity.setDisabled("true");
    	optionEntity.save();	
    }
    
    @Override
    public void require(int id)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	optionEntity.setRequired("true");
    	optionEntity.save();	
    }
    
    @Override
    public void optional(int id)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	optionEntity.setRequired("false");
    	optionEntity.save();	
    }
    
    @Override
    public void update(int id, String value)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	optionEntity.setCustomValue(value);
    	optionEntity.save();	
    }
    
    @Override
    public void updateMapping(int id, String value)
    {
    	MultiCSCFMapping optionEntity = ao.get(MultiCSCFMapping.class,id);
    	optionEntity.setAssignee(value);
    	optionEntity.save();	
    }
    
    @Override
    public MultiCSCFMapping getMapping(Long option)
    {
    	
    	ArrayList<MultiCSCFMapping> mappings = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("OPTION = ?", option).order("SEQUENCE ASC")));
    	for (MultiCSCFMapping mapping : mappings) {
    		return mapping;
        }
    	
    	return null;
    		
    }
    

    
    @Override
    public void sort(Long fieldconfig, Long customfield)
    {
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield).order("CUSTOM_VALUE ASC")));
    	int index = 0;
    	for (MultiCSCFEntity option : options) {
    		option.setSequence(index);
    		option.save();
    		index++;
        }
    	
    }

    @Override	
    public MultiCSCFEntity add(Long fieldconfig, Long customfield, String Value)
    {
    	int count = ao.count(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield));
        final MultiCSCFEntity option = ao.create(MultiCSCFEntity.class);
        option.setCustomField(customfield);
        option.setFieldConfig(fieldconfig);
        option.setSequence(count);   
        option.setCustomValue(Value);
        option.setDisabled("false");
        option.setRequired("false");
        option.save();
        return option;
    }
    
    @Override	
    public MultiCSCFMapping addMapping(Long fieldconfig, Long customfield, Long option, Long parent, String assignee)
    {
    	int count = ao.count(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield));
        final MultiCSCFMapping mapping = ao.create(MultiCSCFMapping.class);
        mapping.setCustomField(customfield);
        mapping.setFieldConfig(fieldconfig);
        mapping.setParentOption(parent);
        mapping.setOption(option);
        mapping.setSequence(count);   
        mapping.setAssignee(assignee);
        mapping.save();
        int sequence = mapping.getSequence();
    	ArrayList<MultiCSCFMapping> options = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE <= ?", fieldconfig, customfield, sequence)));
    	for (MultiCSCFMapping mappingoption : options) {
    		if(mappingoption.getID()==mapping.getID()) {
    			mappingoption.setSequence(0);
    		} else
    		{
    			mappingoption.setSequence(mappingoption.getSequence() + 1);
    		}
    		mappingoption.save();
        }
        return mapping;
    }
    

	@Override
	public MultiCSCFUser addUser(Long fieldconfig, Long customfield, String Value) {
		final MultiCSCFUser user = ao.create(MultiCSCFUser.class);
		user.setUsername(Value);
		user.setCustomField(customfield);
		user.setFieldConfig(fieldconfig);
		user.save();
		
		return user;
	}

    
    @Override	
    public List<MultiCSCFEntity> getMultiCascadeSelectOptions(Long fieldconfig, Long customfield)
    {
    	return newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield).order("SEQUENCE ASC")));
    }
    
    @Override	
    public List<MultiCSCFMapping> getMultiCascadeSelectMappings(Long fieldconfig, Long customfield)
    {
    	return newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield).order("SEQUENCE ASC")));
    }
    
	@Override
	public List<MultiCSCFUser> getMultiCascadeSelectUsers(Long fieldconfig, Long customfield) {
		return newArrayList(ao.find(MultiCSCFUser.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield)));
	}
    
    
    
    @Override	
    public MultiCSCFEntity getOptionBySequence(Long fieldconfig, Long customfield, int sequence)
    {
    	log.debug("fieldconfig" + fieldconfig.toString() + "customfield " + customfield.toString() + "sequence " + String.valueOf(sequence));
    	
    	newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield).order("SEQUENCE ASC")));
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE = ?", fieldconfig, customfield, sequence-1)));
    	for (MultiCSCFEntity option : options) {
    		return option;
        }
    	
    	return null;
    }
    
    @Override	
    public MultiCSCFEntity getOptionByCustomFieldValue(Long customfield, String value)
    {
    	log.debug("customfield " + customfield.toString() + "value " + value);
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("CUSTOM_FIELD = ? AND CUSTOM_VALUE = ?", customfield, value).order("SEQUENCE ASC")));
    	if(options.isEmpty())
    		log.debug("empty options!");
    	
    	for (MultiCSCFEntity option : options) {
    		return option;
        }
    	
    	return null;
    }
    
    
    
    @Override    
    public boolean existsValue(Long fieldconfig, Long customfield, String value){
    	int count = ao.count(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND CUSTOM_VALUE = ?", fieldconfig, customfield, value));
    	if (count >= 1) {
    		return true;
    	} else
    	{
    		return false;
    	}
    	
    }
    

	@Override
	public boolean existsUser(Long fieldconfig, Long customfield, String value) {
		
		int count = ao.count(MultiCSCFUser.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND USERNAME = ?", fieldconfig, customfield, value));
    	if (count >= 1) {
    		return true;
    	} else
    	{
    		return false;
    	}

	}
    
    
    @Override    
	public void moveToLast(Long fieldconfig, Long customfield, int id) 
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	int sequence = optionEntity.getSequence();
    	int count = ao.count(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield));
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE >= ?", fieldconfig, customfield, sequence)));
    	for (MultiCSCFEntity option : options) {
    		if(option.getID()==id) {
    			option.setSequence(count-1);
    		} else
    		{
    		 option.setSequence(option.getSequence() - 1);
    		}
    		option.save();
        }
    }
	
    @Override	
    public void moveUp(Long fieldconfig, Long customfield,int id)
    {
    	log.debug("id " + String.valueOf(id));
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	int sequence = optionEntity.getSequence();
    	optionEntity.setSequence(sequence-1);
    	optionEntity.save();

    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE = ?", fieldconfig, customfield, sequence - 1)));
    	for (MultiCSCFEntity option : options) {
    		if (option.getID()!=optionEntity.getID())
    		{
        		option.setSequence(sequence);
        		option.save();
    		}

    	}
    }
    
    
    @Override
    public void moveDown(Long fieldconfig, Long customfield,int id)
    {
    	log.debug("id " + String.valueOf(id));
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	int sequence = optionEntity.getSequence();
    	int newsequence = sequence + 1;
    	log.debug("newsequence " + String.valueOf(newsequence));
    	optionEntity.setSequence(newsequence);
    	optionEntity.save();
    	log.debug("sequence " + String.valueOf(optionEntity.getSequence()));
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE = ?", fieldconfig, customfield, sequence + 1)));
    	for (MultiCSCFEntity option : options) {
    		if (option.getID()!=optionEntity.getID())
    		{
    		option.setSequence(sequence);
    		option.save();
    		}
    	}
    }
    
    
    @Override
    public void moveToFirst(Long fieldconfig, Long customfield,int id)
    {
    	MultiCSCFEntity optionEntity = ao.get(MultiCSCFEntity.class,id);
    	int sequence = optionEntity.getSequence();
    	ArrayList<MultiCSCFEntity> options = newArrayList(ao.find(MultiCSCFEntity.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE <= ?", fieldconfig, customfield, sequence).order("CUSTOM_VALUE ASC")));
    	for (MultiCSCFEntity option : options) {
    		if(option.getID()==id) {
    			option.setSequence(0);
    		} else
    		{
    		 option.setSequence(option.getSequence() + 1);
    		}
    		option.save();
        }
    }
    
    @Override    
	public void moveToLastMapping(Long fieldconfig, Long customfield, int id) 
    {
    	MultiCSCFMapping optionEntity = ao.get(MultiCSCFMapping.class,id);
    	int sequence = optionEntity.getSequence();
    	int count = ao.count(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ?", fieldconfig, customfield));
    	ArrayList<MultiCSCFMapping> options = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE >= ?", fieldconfig, customfield, sequence)));
    	for (MultiCSCFMapping option : options) {
    		if(option.getID()==id) {
    			option.setSequence(count-1);
    		} else
    		{
    		 option.setSequence(option.getSequence() - 1);
    		}
    		option.save();
        }
    }
	
    @Override	
    public void moveUpMapping(Long fieldconfig, Long customfield,int id)
    {
    	log.debug("id " + String.valueOf(id));
    	MultiCSCFMapping optionEntity = ao.get(MultiCSCFMapping.class,id);
    	int sequence = optionEntity.getSequence();
    	optionEntity.setSequence(sequence-1);
    	optionEntity.save();

    	ArrayList<MultiCSCFMapping> options = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE = ?", fieldconfig, customfield, sequence - 1)));
    	for (MultiCSCFMapping option : options) {
    		if (option.getID()!=optionEntity.getID())
    		{
        		option.setSequence(sequence);
        		option.save();
    		}

    	}
    }
    
    
    @Override
    public void moveDownMapping(Long fieldconfig, Long customfield,int id)
    {
    	log.debug("id " + String.valueOf(id));
    	MultiCSCFMapping optionEntity = ao.get(MultiCSCFMapping.class,id);
    	int sequence = optionEntity.getSequence();
    	int newsequence = sequence + 1;
    	log.debug("newsequence " + String.valueOf(newsequence));
    	optionEntity.setSequence(newsequence);
    	optionEntity.save();
    	log.debug("sequence " + String.valueOf(optionEntity.getSequence()));
    	ArrayList<MultiCSCFMapping> options = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE = ?", fieldconfig, customfield, sequence + 1)));
    	for (MultiCSCFMapping option : options) {
    		if (option.getID()!=optionEntity.getID())
    		{
    		option.setSequence(sequence);
    		option.save();
    		}
    	}
    }
    
    
    @Override
    public void moveToFirstMapping(Long fieldconfig, Long customfield,int id)
    {
    	MultiCSCFMapping optionEntity = ao.get(MultiCSCFMapping.class,id);
    	int sequence = optionEntity.getSequence();
    	ArrayList<MultiCSCFMapping> options = newArrayList(ao.find(MultiCSCFMapping.class,Query.select().where("FIELD_CONFIG = ? AND CUSTOM_FIELD = ? AND SEQUENCE <= ?", fieldconfig, customfield, sequence)));
    	for (MultiCSCFMapping option : options) {
    		if(option.getID()==id) {
    			option.setSequence(0);
    		} else
    		{
    		 option.setSequence(option.getSequence() + 1);
    		}
    		option.save();
        }
    }
    
    
    private final Logger log = Logger.getLogger(MultiCascadeSelectServiceImpl.class);




}